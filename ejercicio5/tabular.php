<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabular</title>
</head>
<body>
    <?php
        $host = "localhost";
        $port = "5432";
        $dbname = "ejercicio4";
        $user = "postgres";
        $password = "mimteg123Pos";

        $conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

        if (!$conn) {
            echo "Error al conectar a la base de datos.";
            exit;
        }

        $query = "SELECT * FROM ejercicio4";
        $result = pg_query($conn, $query);

        if ($result) {
            echo "<table border='1'>";
            echo "<tr><th>ID</th><th>Nombre</th><th>Descripción</th></tr>";
            while ($row = pg_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['nombre'] . "</td>";
                echo "<td>" . $row['descripcion'] . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        } else {
            echo "No se pudieron recuperar los registros.";
        }

        pg_close($conn);
    ?>
</body>
</html>