-- Poblando la tabla Empresa
INSERT INTO public."Empresa" (nombre) VALUES 
('Empresa 1'), 
('Empresa 2'), 
('Empresa 3'), 
('Empresa 4'), 
('Empresa 5');

-- Poblando la tabla Marca
INSERT INTO public."Marca" (id_empresa, nombre) VALUES 
(1, 'Marca 1'), 
(2, 'Marca 2'), 
(3, 'Marca 3'), 
(4, 'Marca 4'), 
(5, 'Marca 5');

-- Poblando la tabla Categoria
INSERT INTO public."Categoria" (nombre) VALUES 
('Categoria 1'), 
('Categoria 2'), 
('Categoria 3'), 
('Categoria 4'), 
('Categoria 5');

-- Poblando la tabla Producto
INSERT INTO public."Producto" (id_marca, id_categoria, nombre, precio) VALUES 
(1, 1, 'Producto 1', 100.0), 
(2, 2, 'Producto 2', 200.0), 
(3, 3, 'Producto 3', 300.0), 
(4, 4, 'Producto 4', 400.0), 
(5, 5, 'Producto 5', 500.0);
