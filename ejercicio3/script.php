<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio3</title>
</head>
<style>
    td, th {
        padding: 10px; /* Ajusta este valor a tus necesidades */
    }
</style>
<body>
    <?php
        // Conexión a la base de datos
        $host = "localhost";
        $port = "5432";
        $dbname = "ejercicio1";
        $user = "postgres";
        $password = "mimteg123Pos";
    
        $conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

    if (!$conn) {
        echo "Error al conectar a la base de datos.";
        exit;
    }

        // Realizar la consulta
        $query = 'SELECT 
                        P.nombre AS nombre_producto, 
                        P.precio AS precio_producto, 
                        M.nombre AS nombre_marca, 
                        E.nombre AS nombre_empresa, 
                        C.nombre AS nombre_categoria
                    FROM 
                        public."Producto" P
                    JOIN 
                        public."Marca" M ON P.id_marca = M.id_marca
                    JOIN 
                        public."Empresa" E ON M.id_empresa = E.id_empresa
                    JOIN 
                        public."Categoria" C ON P.id_categoria = C.id_categoria;
                ';
        $result = pg_query($query) or die('La consulta fallo: ' . pg_last_error());

        // Imprimir los resultados en HTML
        echo "<table>\n";
        echo "<tr>
                <th>Nombre del producto</th>
                <th>Precio del producto</th>
                <th>Nombre de la marca</th>
                <th>Nombre de la empresa</th>
                <th>Nombre de la categoría</th>
            </tr>";
        while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
            echo "\t<tr>\n";
            foreach ($line as $col_value) {
                echo "\t\t<td>$col_value</td>\n";
            }
            echo "\t</tr>\n";
        }
        echo "</table>\n";

        // Liberar resultados
        pg_free_result($result);

        // Cerrar la conexión
        pg_close($dbconn);
    ?>
    
</body>
</html>