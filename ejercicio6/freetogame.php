<!DOCTYPE html>
<html>
<head>
    <title>FreeToGame API Data</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            width: 80%;
            margin: auto;
            overflow: hidden;
        }
        .card {
            background: #fff;
            border-radius: 10px;
            padding: 20px;
            margin-bottom: 20px;
            box-shadow: 0 5px 15px rgba(0,0,0,0.1);
        }
        .card img {
            max-width: 100%;
            height: auto;
            border-radius: 5px;
        }
        .card h2 {
            margin-top: 0;
        }
        .card p {
            color: #333;
        }
        .card a {
            display: inline-block;
            text-decoration: none;
            color: white;
            background-color: #007bff;
            padding: 10px 15px;
            border-radius: 5px;
            margin-top: 10px;
        }
        .card a:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>FreeToGame API Data</h1>
        <?php
        $apiUrl = 'https://www.freetogame.com/api/games';
        $response = file_get_contents($apiUrl);
        $data = json_decode($response);

        if ($data) {
            foreach ($data as $item) {
                echo '<div class="card">';
                echo '<img src="' . $item->thumbnail . '" alt="' . $item->title . '">';
                echo '<h2>' . $item->title . '</h2>';
                echo '<p>' . $item->short_description . '</p>';
                echo '<p><strong>Category:</strong> ' . $item->genre . '</p>';
                echo '<p><strong>Platform:</strong> ' . $item->platform . '</p>';
                echo '<p><strong>Publisher:</strong> ' . $item->publisher . '</p>';
                echo '<p><strong>Release Date:</strong> ' . $item->release_date . '</p>';
                echo '<a href="' . $item->game_url . '" target="_blank">More Info</a>';
                echo '</div>';
            }
        } else {
            echo '<p>Unable to fetch data from the API.</p>';
        }
        ?>
    </div>
</body>
</html>
